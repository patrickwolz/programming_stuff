class Wand{
  PVector pos1;
  PVector pos2;
  int hue;
  public Wand(PVector p1, PVector p2, color _hue){
    pos1 = p1;
    pos2 = p2;
    hue = _hue;
  }
  public void show2D(){
    stroke(hue,200,200);
    strokeWeight(2);
    line(pos1.x, pos1.y, pos2.x, pos2.y);
  }
}
