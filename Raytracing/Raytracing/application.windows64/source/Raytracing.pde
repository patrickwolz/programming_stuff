import java.awt.Robot;

Wand[] walls = {
  //Außenwände
  new Wand(new PVector(0, 0), new PVector(0, 400), 0), 
  new Wand(new PVector(0, 0), new PVector(400, 0), 0), 
  new Wand(new PVector(0, 400), new PVector(400, 400), 0), 
  new Wand(new PVector(400, 0), new PVector(400, 400), 0), 

  //Mitte
  new Wand(new PVector(100, 100), new PVector(300, 100), 190), 
  new Wand(new PVector(300, 200), new PVector(300, 300), 70), 
  new Wand(new PVector(200, 200), new PVector(300, 200), 130), 
  new Wand(new PVector(100, 300), new PVector(200, 300), 240),
  
  new Wand(new PVector(random(400),random(400)), new PVector(random(400), random(400)), int(random(255))),
};
Particle p;
Robot robot;

void setup() {
  //size(1600, 900);
  fullScreen();
  noCursor();
  colorMode(HSB);

  try
  {
    robot = new Robot();
  }
  catch (Exception e)
  {
    println("Robot class not supported by your system!");
    exit();
  }
  p = new Particle(70);
}
void draw() {
  background(0);
  for (Wand wall : walls) {
    wall.show2D();
  }
  p.show();
  p.look(walls);
  if (keyPressed) {
    switch(key) {
    case 'w':
      p.pos.add(p.dir);
      break;
    case 's':
      p.pos.sub(p.dir);
      break;
    case 'a':
      p.pos.add(new PVector(p.dir.y, -p.dir.x));
      break;
    case 'd':
      p.pos.add(new PVector(-p.dir.y, p.dir.x));
      break;
    }
  }

  stroke(0, 0, 355);
  line(p.pos.x, p.pos.y, p.pos.x+p.dir.x * 20, p.pos.y+p.dir.y * 20);

  checkMouse();
  
  rectMode(CORNER);
  fill(0);
  noStroke();
  rect(0,560,1600,700);
  noFill();
  stroke(255);
  rect(500, 0, 1000, 560);
}

public void checkMouse() {
  if (mouseX>=1500) {
    robot.mouseMove(501, mouseY);
  } else if (mouseX <= 500) {
    robot.mouseMove(1499, mouseY);
  }
  if (mouseY>560) {
    robot.mouseMove(mouseX, 560);
  }
  if (pmouseX>mouseX && pmouseX <= 1500) {
    p.newRays(map(mouseX, 500, 1500, 0, 360));
    p.dir = PVector.fromAngle(radians(map(mouseX, 500, 1500, 0, 360)));
  } else if (pmouseX<mouseX && pmouseX >= 500) {
    p.newRays(map(mouseX, 500, 1500, 0, 360));
    p.dir = PVector.fromAngle(radians(map(mouseX, 500, 1500, 0, 360)));
  }
}
void mousePressed(){
  robot.mouseMove(500, 280);
}
