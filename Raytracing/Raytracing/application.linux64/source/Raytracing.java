import processing.core.*; 
import processing.data.*; 
import processing.event.*; 
import processing.opengl.*; 

import java.awt.Robot; 

import java.util.HashMap; 
import java.util.ArrayList; 
import java.io.File; 
import java.io.BufferedReader; 
import java.io.PrintWriter; 
import java.io.InputStream; 
import java.io.OutputStream; 
import java.io.IOException; 

public class Raytracing extends PApplet {



Wand[] walls = {
  //Außenwände
  new Wand(new PVector(0, 0), new PVector(0, 400), 0), 
  new Wand(new PVector(0, 0), new PVector(400, 0), 0), 
  new Wand(new PVector(0, 400), new PVector(400, 400), 0), 
  new Wand(new PVector(400, 0), new PVector(400, 400), 0), 

  //Mitte
  new Wand(new PVector(100, 100), new PVector(300, 100), 190), 
  new Wand(new PVector(300, 200), new PVector(300, 300), 70), 
  new Wand(new PVector(200, 200), new PVector(300, 200), 130), 
  new Wand(new PVector(100, 300), new PVector(200, 300), 240),
  
  new Wand(new PVector(random(400),random(400)), new PVector(random(400), random(400)), PApplet.parseInt(random(255))),
};
Particle p;
Robot robot;

public void setup() {
  //size(1600, 900);
  
  noCursor();
  colorMode(HSB);

  try
  {
    robot = new Robot();
  }
  catch (Exception e)
  {
    println("Robot class not supported by your system!");
    exit();
  }
  p = new Particle(70);
}
public void draw() {
  background(0);
  for (Wand wall : walls) {
    wall.show2D();
  }
  p.show();
  p.look(walls);
  if (keyPressed) {
    switch(key) {
    case 'w':
      p.pos.add(p.dir);
      break;
    case 's':
      p.pos.sub(p.dir);
      break;
    case 'a':
      p.pos.add(new PVector(p.dir.y, -p.dir.x));
      break;
    case 'd':
      p.pos.add(new PVector(-p.dir.y, p.dir.x));
      break;
    }
  }

  stroke(0, 0, 355);
  line(p.pos.x, p.pos.y, p.pos.x+p.dir.x * 20, p.pos.y+p.dir.y * 20);

  checkMouse();
  
  rectMode(CORNER);
  fill(0);
  noStroke();
  rect(0,560,1600,700);
  noFill();
  stroke(255);
  rect(500, 0, 1000, 560);
}

public void checkMouse() {
  if (mouseX>=1500) {
    robot.mouseMove(501, mouseY);
  } else if (mouseX <= 500) {
    robot.mouseMove(1499, mouseY);
  }
  if (mouseY>560) {
    robot.mouseMove(mouseX, 560);
  }
  if (pmouseX>mouseX && pmouseX <= 1500) {
    p.newRays(map(mouseX, 500, 1500, 0, 360));
    p.dir = PVector.fromAngle(radians(map(mouseX, 500, 1500, 0, 360)));
  } else if (pmouseX<mouseX && pmouseX >= 500) {
    p.newRays(map(mouseX, 500, 1500, 0, 360));
    p.dir = PVector.fromAngle(radians(map(mouseX, 500, 1500, 0, 360)));
  }
}
public void mousePressed(){
  robot.mouseMove(500, 280);
}
class Particle{
  PVector pos;
  PVector dir;
  int fov;
  Ray[] rays;
  
  public Particle(int _fov){
    pos = new PVector(50,50);
    dir = new PVector(0,0);
    fov = _fov;
    rays = new Ray[fov];
    for(int i = - fov/2 ; i<fov/2 ; i++){
      rays[i+fov/2] = new Ray(pos,radians(i));
    }
  }
  
  public void look(Wand[] walls) {
    for (int i = 0; i < rays.length; i++) {
      Ray ray = rays[i];
      PVector closest = null;
      float record = 500000000;
      int col = color(0,0,255);
      for (Wand wall : walls) {
        PVector pt = ray.cast(wall);
        if (pt != null) {
          PVector temp = new PVector(pt.x,pt.y);
          float d = PVector.dist(this.pos, temp);
          if (d < record) {
            record = d;
            closest = temp;
            col = color(pt.z,200,map(d,0,450,255,0));
          }
        }
      }
      if (closest != null) {
        stroke(255, 100);
        line(this.pos.x, this.pos.y, closest.x, closest.y);
        noStroke();
        fill(col);
        rectMode(CENTER);
        rect(map(i,0,rays.length,500,1500)+1000/rays.length * 0.5f,map(mouseY,0,560,1000,-200),1000/rays.length,1/(20*record) * 500000);
      }
    }
  }
  
  public void show() {
    fill(255);
    ellipse(pos.x, pos.y, 4, 4);
    for (Ray ray : rays) {
      ray.show();
    }
  }
  
  public void newRays(float angle){
    for(int i = - fov/2 ; i<fov/2 ; i++){
      rays[i+fov/2] = new Ray(pos,radians(i + angle));
    }
  }
}
class Ray{
  PVector pos;
  PVector dir;
  public Ray(PVector _pos, float angle){
    pos = _pos;
    dir = PVector.fromAngle(angle);
  }
  public void show() {
    stroke(255);
    pushMatrix();
    translate(pos.x, pos.y);
    line(0, 0, dir.x * 10, dir.y * 10);
    popMatrix();
  }
  public PVector cast(Wand wall) {
    float x1 = wall.pos1.x;
    float y1 = wall.pos1.y;
    float x2 = wall.pos2.x;
    float y2 = wall.pos2.y;

    float x3 = pos.x;
    float y3 = pos.y;
    float x4 = pos.x + dir.x;
    float y4 = pos.y + dir.y;

    float den = (x1 - x2) * (y3 - y4) - (y1 - y2) * (x3 - x4);
    if (den == 0) {
      return null;
    }

    float t = ((x1 - x3) * (y3 - y4) - (y1 - y3) * (x3 - x4)) / den;
    float u = -((x1 - x2) * (y1 - y3) - (y1 - y2) * (x1 - x3)) / den;
    if (t > 0 && t < 1 && u > 0) {
      PVector pt = new PVector();
      pt.x = x1 + t * (x2 - x1);
      pt.y = y1 + t * (y2 - y1);
      pt.z = wall.hue;
      return pt;
    } else {
      return null;
    }
  }
}
class Wand{
  PVector pos1;
  PVector pos2;
  int hue;
  public Wand(PVector p1, PVector p2, int _hue){
    pos1 = p1;
    pos2 = p2;
    hue = _hue;
  }
  public void show2D(){
    stroke(hue,200,200);
    strokeWeight(2);
    line(pos1.x, pos1.y, pos2.x, pos2.y);
  }
}
  public void settings() {  fullScreen(); }
  static public void main(String[] passedArgs) {
    String[] appletArgs = new String[] { "Raytracing" };
    if (passedArgs != null) {
      PApplet.main(concat(appletArgs, passedArgs));
    } else {
      PApplet.main(appletArgs);
    }
  }
}
