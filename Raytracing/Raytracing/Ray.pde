class Ray{
  PVector pos;
  PVector dir;
  public Ray(PVector _pos, float angle){
    pos = _pos;
    dir = PVector.fromAngle(angle);
  }
  public void show() {
    stroke(255);
    pushMatrix();
    translate(pos.x, pos.y);
    line(0, 0, dir.x * 10, dir.y * 10);
    popMatrix();
  }
  public PVector cast(Wand wall) {
    float x1 = wall.pos1.x;
    float y1 = wall.pos1.y;
    float x2 = wall.pos2.x;
    float y2 = wall.pos2.y;

    float x3 = pos.x;
    float y3 = pos.y;
    float x4 = pos.x + dir.x;
    float y4 = pos.y + dir.y;

    float den = (x1 - x2) * (y3 - y4) - (y1 - y2) * (x3 - x4);
    if (den == 0) {
      return null;
    }

    float t = ((x1 - x3) * (y3 - y4) - (y1 - y3) * (x3 - x4)) / den;
    float u = -((x1 - x2) * (y1 - y3) - (y1 - y2) * (x1 - x3)) / den;
    if (t > 0 && t < 1 && u > 0) {
      PVector pt = new PVector();
      pt.x = x1 + t * (x2 - x1);
      pt.y = y1 + t * (y2 - y1);
      pt.z = wall.hue;
      return pt;
    } else {
      return null;
    }
  }
}