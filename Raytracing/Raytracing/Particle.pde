class Particle{
  PVector pos;
  PVector dir;
  int fov;
  Ray[] rays;
  
  public Particle(int _fov){
    pos = new PVector(50,50);
    dir = new PVector(0,0);
    fov = _fov;
    rays = new Ray[fov];
    for(int i = - fov/2 ; i<fov/2 ; i++){
      rays[i+fov/2] = new Ray(pos,radians(i));
    }
  }
  
  void look(Wand[] walls) {
    for (int i = 0; i < rays.length; i++) {
      Ray ray = rays[i];
      PVector closest = null;
      float record = 500000000;
      color col = color(0,0,255);
      for (Wand wall : walls) {
        PVector pt = ray.cast(wall);
        if (pt != null) {
          PVector temp = new PVector(pt.x,pt.y);
          float d = PVector.dist(this.pos, temp);
          if (d < record) {
            record = d;
            closest = temp;
            col = color(pt.z,200,map(d,0,450,255,0));
          }
        }
      }
      if (closest != null) {
        stroke(255, 100);
        line(this.pos.x, this.pos.y, closest.x, closest.y);
        noStroke();
        fill(col);
        rectMode(CENTER);
        rect(map(i,0,rays.length,500,1500)+1000/rays.length * 0.5,map(mouseY,0,560,1000,-200),1000/rays.length,1/(20*record) * 500000);
      }
    }
  }
  
  void show() {
    fill(255);
    ellipse(pos.x, pos.y, 4, 4);
    for (Ray ray : rays) {
      ray.show();
    }
  }
  
  public void newRays(float angle){
    for(int i = - fov/2 ; i<fov/2 ; i++){
      rays[i+fov/2] = new Ray(pos,radians(i + angle));
    }
  }
}